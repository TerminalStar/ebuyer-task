#README for Ebuyer Placement Task

This repository contains my solution to the presented task. It has the abstract base class,
a concrete class to demonstrate an implemented sorting algorithm (bubblesort) as well as a
JUnit test class to show it works correctly.

The files were created in Eclipse IDE.