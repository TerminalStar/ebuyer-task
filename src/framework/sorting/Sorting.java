package framework.sorting;

/**
 * Abstract class containing shared checkSort function (to ensure an array is 
 * sorted) and sort function.
 * 
 * @param numbers	The unsorted input array.
 * @param done		Whether the array is sorted or not.
 * @param count		Counter to track how many elements are correct.
 */

public abstract class Sorting{
	public static int[] numbers;
	public static boolean done = false;
	public static int count = 0;
	
	/**
	 * Function to see whether a given array is sorted. Used to check the arrays once
	 * the sort function has run.
	 * 
	 * @param numbers	An array that has been through the sort function.
	 * 
	 * @return 			The "done" variable of an array, either true (sorted)
	 * 					or false (unsorted).
	 */
	
	static boolean checkSort(int[] numbers){
		for(int i=0; i < numbers.length-1; i++){
			if(numbers[i] <= numbers[i+1]){
				count++;
			}
		}
		
		if(count == numbers.length-1)
			done = true;
		else
			done = false;
		
		return done;
	}
	
	/**
	 * Abstract function to implement a sorting algorithm.
	 */
	
	abstract int[] sort(int[] numbers);
}
