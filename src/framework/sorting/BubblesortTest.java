package framework.sorting;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class to ensure Bubblesort.java works as expected. Runs on the unsorted array
 * {22,1,9,34}.
 */

public class BubblesortTest{
	
	/**
	 * Testing function.
	 * 
	 * @param tester	Bubblesort object, to test the sort function.
	 * @param test		Unsorted array to input.
	 * @param correct	The correct version of the array; what the result is 
	 * 					expected to be.
	 * @param result	The actual result from calling sort() on test.
	 * 
	 * @return			Whether result is equal to correct. If not, the test fails.
	 * 					Also outputs the input, correct and generated arrays.
	 */
	
	@Test
	public void test(){
		Bubblesort tester = new Bubblesort();
		int[] test		= {22,1,9,34};
		int[] correct	= {1,9,22,34};
		int[] result	= tester.sort(test);
		Assert.assertArrayEquals(correct,result);
		System.out.println(	"Input array = " + Arrays.toString(test) + ", correct array = " + Arrays.toString(correct) + 
							", generated array = " + Arrays.toString(result) + "\n\r");
	}

}
