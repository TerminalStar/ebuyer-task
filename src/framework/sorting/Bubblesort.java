package framework.sorting;

/**
 * Class to implement a bubble sort on a given array.
 * 
 * @param temp	A variable to store an array value, used when switching a value's
 * 				index.
 */

public class Bubblesort extends Sorting{
	
	private int temp;
	
	/**
	 * Implemented sort function. It will compare each element to the one after, until 
	 * done == true, meaning the array is sorted. Checks to ensure the array is indeed
	 * sorted.
	 * 
	 * Returns the sorted array.
	 * 
	 * @param numbers	Unsorted array.
	 * @param i			Loop counter.
	 * 
	 * @return numbers	Sorted version of the input array.	
	 */

	@Override
	public int[] sort(int[] numbers){
		int i = 0;
		
		while(done == false){
			if(numbers.length <= 1)
				done = true;
			else{
				for(; i < numbers.length-1; i++){
					if(numbers[i] > numbers[i+1]){
						temp = numbers[i];
						numbers[i] = numbers[i+1];
						numbers[i+1] = temp;
					}
				}
			}
			
			Sorting.checkSort(numbers);
		}
	return numbers;
	}

}
